import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue'
import PlantDatabase from '../views/PlantDatabase.vue'
import Detail from '../views/Detail.vue'
import HerbariumDatabase from '../views/HerbariumDatabase.vue'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/PlantDatabase',
    name: 'PlantDatabase',
    component: PlantDatabase,
  }, {
    path: '/PlantDatabase/Detail',
    name: 'Detail',
    component: Detail,
  },
  {
    path: '/HerbariumDatabase',
    name: 'HerbariumDatabase',
    component: HerbariumDatabase,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
